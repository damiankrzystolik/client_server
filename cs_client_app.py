


import socket
import json
from cs_client_class import Client
from icecream import ic
ic.configureOutput(prefix='Client app - ')



client = Client()
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
    sock.connect((client.host, client.port))
    try:
        while client.client_connected:
            client.send_to_server(sock)
            response = client.receive_from_server(sock)
            command_handlers = {
                "del_msg": client.delete_msg,
                "reg": client.reg,
                "login": client.login,
                "stop": lambda sock: setattr(client, "client_connected", False),
                "del": client.delete_user,
                "send_message": client.message
            }

            handler = command_handlers.get(response["response_cmd"])
            if handler:
                handler(sock)
                client.receive_from_server(sock)

    except ConnectionRefusedError:
        client.log.error(f"ConnectionRefusedErrof to {client.host}:{client.port}", exc_info=True)
    except json.JSONDecodeError:
        if client.client_connected:
            client.log.error("json error", exc_info=False)
    except Exception:
        client.log.error("Exception from Client App", exc_info=True)
    finally:
        sock.close()
