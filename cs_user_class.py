class User:
    """Użytkownik serwera. Na razie jest tylko jeden dla jednego połączenia.
    Potem zobaczymy czy wykorzystam to dla większej ilosci połączeń"""

    def __init__(self):
        self.username = "Guest"
        self.password = None
        self.role = None
        self.logged = False

    def __str__(self):
        return f"User: {self.username}\nRole: {self.role}\nLogged: {self.logged}\n"