# cs_server_class.py


import socket
import json
import time
import bcrypt
from stuff.db_class import DataBase
from cs_user_class import User
from cs_db_sqlite_class import DataBaseSQLITE
# from sqlite_helper import DataBaseSQLITE

import logging
logging.basicConfig(
    filename="logs_file.log",
    encoding="utf-8",
    filemode="a",
    format="{asctime} - {levelname} - {name} - {message}",
    style="{",
    datefmt="%Y-%m-%d %H:%M:%S",
    level=logging.DEBUG,
)

from icecream import ic
ic.configureOutput(prefix='Server class - ')

with open('config.json') as config_file:
    config = json.load(config_file)

HOST = config['host']
PORT = config['port']
SERVER_VERSION = config['server_version'] # wersja z sqlite


class Server:
    def __init__(self, host=HOST, port=PORT, logger_name='Server'):
        self.host = host
        self.port = port
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.running = True
        self.start_time = time.time()
        self.client = User()  # serwer ma tylko jednego klienta ale to pewnie sie zmieni
        # self.db = DataBase()
        self.sqlite = DataBaseSQLITE()

        self.tables = self.sqlite.create_tables()
        self.conn = self.sqlite.insert_admin()

        self.log = logging.getLogger(logger_name)
        self.log.info(f"Started")

    @staticmethod
    def _read(client_socket):
        """An auxiliary function that handles receiving messages from a client"""
        data = client_socket.recv(1024).decode("utf-8")
        if not data:
            return
        request = json.loads(data)
        print(f"Otrzymano komende: {request['cmd']}\n")
        return request

    def _write(self, request, client_socket):
        """An auxiliary function that handles sending messages to the client"""
        if self.client.logged is True:
            response = self._create_response_logged(request)
        elif self.client.logged is False:
            response = self._create_response_logout(request)
        return client_socket.sendall(json.dumps(response).encode("utf-8"))

    def client_request(self, client_socket):
        """Main function for receiving/sending messages"""
        request = self._read(client_socket)
        self._write(request, client_socket)
        return request

    def _create_response_logged(self, request):
        """A function that handles responses if the User is logged in"""
        # słownik komend do aktywacji funkcji
        commands = {
            "delete_msg": lambda r: self._handle_delete_msg(),
            "delete_message": lambda r: self._handle_delete_messages(r),
            "send_msg": lambda r: self._handle_send_msg(),
            "receive_message": lambda r: self._handle_receive_message(r),
            "users": lambda r: self._handle_users(),
            "delete": lambda r: self._handle_delete(),
            "del": lambda r: self._handle_del(r),
            "inbox": lambda r: self._handle_inbox(),
            "inbox_all": lambda r: self._handle_inbox_all(),
            "help": lambda r: self._handle_help(),
            "logout": lambda r: self._handle_logout(),
            "stop": lambda r: self._handle_stop(),
            "info": lambda r: self._handle_info(),
            "uptime": lambda r: self._handle_uptime(),
        }

        if request["cmd"] not in commands:
            return self.unknown_cmd()

        handler = commands.get(request["cmd"])
        return handler(request)

    def _create_response_logout(self, request):
        """A function that handles responses if the user isn't logged in"""
        commands = {
            "reg": lambda r: self._handle_reg(),
            "register": lambda r: self._handle_register(r),
            "login": lambda r: self._handle_login(),
            "log": lambda r: self._handle_log(r),
            "info": lambda r: self._handle_info(),
            "help": lambda r: self._handle_help(),
            "uptime": lambda r: self._handle_uptime(),
            "stop": lambda r: self._handle_stop(),
        }

        if request["cmd"] not in commands:
            return self.unknown_cmd()

        handler = commands.get(request["cmd"])
        return handler(request)

    def _handle_delete_msg(self):
        """A function that handles deleting messages - give choice for user which messages should be deleted"""
        function_response = self.sqlite.print_msg_sqlite(self.client.username)
        if function_response is False:
            response = {"response": f"Brak wiadomosci.", "response_cmd": ""}
        elif function_response[0] is True:
            response = {
                "response": f"Wpisz 'id' wiadomosci do usunięcia,\n"
                            f"'all' aby usunąć wszystkie wiadomosci'\n"
                            f"'> liczba dni' aby usunąć wiadomosci starsze niż 'liczba dni'.",
                "response_cmd": "del_msg",
            }
        else:
            response = {
                "response": f"Wystąpił błąd {function_response}",
                "response_cmd": "",
            }
        return response

    def _handle_delete_messages(self, request):
        """Function supporting deletion of messages - selects the appropriate function according to the user's choice"""
        all_or_id = request["all_or_id"]
        if all_or_id == 'all':
            if self.sqlite.delete_all_msg_of_user_sqlite(self.client.username) is True:
                response = {"response": f"Usunięto wiadomosc(i)", "response_cmd": ""}
            else:
                response = {"response": f"Wiadomosci nie zostały usunięte.\n"
                                        f"Wytąpił jakis błąd lub żle wprowadzono dane.",
                            "response_cmd": ""}
            return response
        aoi = all_or_id.replace(' ', '')
        aoil = list(aoi)
        days = ''.join(aoil[1:])
        days_str = '-' + days + ' day'

        if aoil[0] == '>':
            if self.sqlite.delete_msg_older_than_sqlite(self.client.username, days_str) is True:
                response = {"response": f"Usunięto wiadomosc(i)", "response_cmd": ""}
            else:
                response = {"response": f"Wiadomosci nie zostały usunięte.\n"
                                        f"Wytąpił jakis błąd lub żle wprowadzono dane.",
                            "response_cmd": ""}
            return response
        else:
            if self.sqlite.delete_msg_by_id_sqlite(self.client.username, all_or_id) is True:
                response = {"response": f"Usunięto wiadomosc(i)", "response_cmd": ""}
            else:
                response = {"response": f"Wiadomosci nie zostały usunięte.\n"
                                        f"Wytąpił jakis błąd lub żle wprowadzono dane.",
                            "response_cmd": ""}
            return response

    @staticmethod
    def _handle_send_msg():
        """A function that handles sending 'sending message' command to the client app"""
        response = {
            "response": "Wyslij wiadomosc do...",
            "response_cmd": "send_message",
        }
        return response

    def _handle_receive_message(self, request):
        """A function that handles receiving message to save from the client app"""
        username = request["to_user"]
        message = request["message"]
        if self.sqlite.check_user(username):
            if self.sqlite.save_message(self.client.username, message, username):
                response = {"response": "Wiadomosc wysłana", "response_cmd": ""}
                return response
            else:
                response = {
                    "response": f"""Wiadomosc nie została wysłana.""",
                    "response_cmd": "",
                }
                return response
        else:
            response = {
                "response": f"""Nieznany użytkownik""",
                "response_cmd": "",
            }
            return response

    def _handle_users(self):
        """A function that handle display all users of server"""
        response = {
            "response": f"\nUżytkownicy\n{self.sqlite.print_user_sqlite()}",
            "response_cmd": "",
        }
        return response

    def _handle_delete(self):
        """A function that handle sending 'delete user' command to the client app"""
        if self.client.role == 0:
            response = {
                "response": f"""\nWybierz użytkownika do usunięcia\n{self.sqlite.print_user_sqlite()}""",
                "response_cmd": "del",
            }
        else:
            response = self.unknown_cmd()
        return response

    def _handle_del(self, request):
        """A function that handle 'delete user' process."""
        user = request["user"]
        delete_user = self.sqlite.check_user_role(user, 1) # role=0 is admin, 1=user.
        if delete_user:
            self.sqlite.delete_user_sqlite(user)
            response = {"response": "Użytkownik usunięty", "response_cmd": ""}
        else:
            response = {
                "response": "Nieznaleziono użytkownika o takim loginie lub użytkownik jest adminem",
                "response_cmd": "",
            }
        return response

    def _handle_help(self):
        if self.client.role == 0:
            response = {
                "response": f"""Dostępne komendy. 
                   info - wersja serwera 
                   help - dostępne komendy serwera 
                   uptime - czas życia serwera 
                   stop - zatrzymuje serwer oraz klienta 
                   users - wyswietla użytkowników serwera 
                   delete - usuń użytkownika 
                   send_msg - wyslij wiadomosc (do 255 znaków) 
                   inbox - skrzynka odbiorcza 
                   inbox_all - wszystkie wiadomosci użytkowników 
                   delete_msg - usuń wiadomosc(i) 
                   logout - wylogowanie""",
                "response_cmd": "",
            }
        elif self.client.role == 1:
            response = {
                "response": f"""Dostępne komendy. 
                   info - wersja serwera 
                   help - dostępne komendy serwera 
                   uptime - czas życia serwera 
                   stop - zatrzymuje serwer oraz klienta 
                   users - wyswietla użytkowników serwera 
                   send_msg - wyslij wiadomosc (do 255 znaków) 
                   inbox - Skrzynka odbiorcza 
                   delete_msg - usuń wiadomosc(i) 
                   logout - wylogowanie""",
                "response_cmd": "",
            }
        else:
            response = {
                "response": f"""Dostępne komendy.
                   info - wersja serwera
                   help - dostępne komendy serwera
                   uptime - czas życia serwera
                   stop - zatrzymuje serwer oraz klienta
                   reg - rejestracja nowego użytkownika
                   login - zalogowanie""",
                "response_cmd": "",
            }
        return response

    def _handle_logout(self):
        self.log.info(f'User {self.client.username} logged out')
        self.client.username = "Guest"
        self.client.password = None
        self.client.role = None
        self.client.logged = False
        response = {"response": "Wylogowano", "response_cmd": ""}
        return response

    def _handle_inbox(self):
        function_response = self.sqlite.print_msg_sqlite(self.client.username)
        if function_response is False:
            response = {"response": 'Brak wiadomosci', "response_cmd": ""}
        elif function_response[0] is True:
            response = {"response": f'{function_response[1]}', "response_cmd": ""}
        else:
            response = {"response": f'Wystąpił błąd', "response_cmd": ''}
        return response

    def _handle_inbox_all(self):
        function_response = self.sqlite.print_msg_sqlite(self.client.username, True)
        if self.client.role == 0:
            if function_response is False:
                response = {"response": 'Brak wiadomosci', "response_cmd": ""}
            elif function_response[0] is True:
                response = {"response": f'{function_response[1]}', "response_cmd": ""}
            else:
                response = {"response": f'Wystąpił błąd', "response_cmd": ''}
            return response


    @staticmethod
    def _handle_reg():
        """A function that handle sending command to user app about registration"""
        response = {
            "response_cmd": "reg",
            "response": "Rejestracja nowego użytkownika.",
        }
        return response

    def _handle_register(self, request):
        """A function that handle registration proces"""
        username = request["user"]
        password = request["password"]
        role = request["role"]
        hashed_password = str(self.hash_password(password).decode("utf-8"))  # hashowanie hasła
        insert_user = self.sqlite.insert_user_sqlite(username, hashed_password, role)
        if insert_user is True:
            response = {"response": "Zapisano nowego użytkownika.", "response_cmd": ""}
            return response
        elif insert_user == 'UNIQUE constraint failed: users.user_name':
            response = {
                "response": "Podanana nazwa uzytkownika już istnieje. Wybierz inną.",
                "response_cmd": "",
            }
            return response
        elif insert_user is False:
            response = {"response": f"{insert_user}", "response_cmd": ""}
            return response

    @staticmethod
    def _handle_login():
        """A function that handle sending command to user app about login"""
        response = {"response": "Wpisz login i hasło.", "response_cmd": "login"}
        return response

    def _handle_log(self, request):
        """A function that handle login user"""
        username = request["user"]
        password = request["password"]
        login = self.sqlite.log_user_sqlite(username, password)
        if login:
            self.client.logged = True
            self.client.role = login[1][2]
            self.client.username = login[1][0]
            response = {
                "response": "Zalogowano",
                "response_cmd": "",
            }
            return response
        else:
            response = {"response": "Niepoprawny login i/lub hasło", "response_cmd": ""}
            return response

    def _handle_info(self):
        """A function handles display information about server and loged user."""
        response = {
            "response": f"""Wersja serwera - {SERVER_VERSION}\n{self.client.__str__()}""",
            "response_cmd": "",
        }
        return response

    def _handle_uptime(self):
        """A function that handles server operating time"""
        current_time = time.time()
        uptime_seconds = int(current_time - self.start_time)
        uptime_string = self.format_uptime(uptime_seconds)
        return {"response": f"Upłyneło: {uptime_string}", "response_cmd": ""}

    def _handle_stop(self):
        """A function that handles server shutdown"""
        response = {
            "response_cmd": "stop",
            "response": "Klient zakończył połączenie.\n",
        }
        self.running = False
        return response

    @staticmethod
    def format_uptime(seconds):
        """A function that formats the display of server operating time"""
        minutes, seconds = divmod(seconds, 60)
        hours, minutes = divmod(minutes, 60)
        days, hours = divmod(hours, 24)

        uptime_parts = []
        if days > 0:
            uptime_parts.append(f"{days}d")
        if hours > 0:
            uptime_parts.append(f"{hours}h")
        if minutes > 0:
            uptime_parts.append(f"{minutes}m")
        uptime_parts.append(f"{seconds}s")

        return " ".join(uptime_parts)


    def unknown_cmd(self):
        """Auxiliary response function if the command is unknown"""
        self.log.warning("User write unknown command")
        response = {"response": "Nieznane polecenie", "response_cmd": ""}
        return response

    @staticmethod
    def hash_password(password):  # hashowanie haseł bcrypt - z pomocą gpt
        """Hash the password"""
        # Generate a salt
        salt = bcrypt.gensalt()
        # Hash the password with the salt
        hashed = bcrypt.hashpw(password.encode("utf-8"), salt)
        return hashed

    @staticmethod
    def check_password(hashed_password, user_password):  # hashowanie haseł bcrypt
        """Check if the provided password matches the hashed password"""
        return bcrypt.checkpw(user_password.encode("utf-8"), hashed_password)


