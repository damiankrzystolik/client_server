


from cs_server_class import Server

server = Server()
server.server_socket.bind((server.host, server.port))
server.server_socket.listen()
print("Czekam na połączenie od Klienta...")
client_socket, client_address = server.server_socket.accept()
print(f"Połączono z {client_address[0]} - {client_address[1]} ")
server.log.info(f'App is running')
try:
    while server.running:
        request = server.client_request(client_socket)
except Exception:
    server.log.error("Exception from Server", exc_info=True)
finally:
    server.server_socket.close()
    server.log.info(f'Server closed\n')
    print("Klient zakończył połączenie. Zamykam serwer.")
