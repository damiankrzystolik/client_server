import sqlite3
import bcrypt
from cs_user_class import User

import json
with open('config.json') as config_file:
    config = json.load(config_file)
DATABASE = config['database']

import logging
logging.basicConfig(
    filename="logs_file.log",
    encoding="utf-8",
    filemode="a",
    format="{asctime} - {levelname} - {name} - {message}",
    style="{",
    datefmt="%Y-%m-%d %H:%M:%S",
    level=logging.DEBUG,
)

class DataBaseSQLITE:
    def __init__(self, logger_name="SqliteDB"):
        self.database = DATABASE
        self.client = User()
        self.log = logging.getLogger(logger_name)
        self.log.info("Database connection established")

    @staticmethod
    def hash_password(password):  # hashowanie haseł bcrypt - z pomocą gpt
        """Hash the password"""
        # Generate a salt
        salt = bcrypt.gensalt()
        # Hash the password with the salt
        hashed = bcrypt.hashpw(password.encode("utf-8"), salt)
        return hashed

    @staticmethod
    def check_password(hashed_password, user_password):  # hashowanie haseł bcrypt
        """Check if the provided password matches the hashed password"""
        return bcrypt.checkpw(user_password.encode("utf-8"), hashed_password)

    # Helper czy tam wraper do kilku najprostszych funkcji zeby sie nie powtarzać !!!DRY!!!
    def _execute_sql(self, sql, values=(), fetchone=False):
        try:
            with sqlite3.connect(self.database) as conn:
                cursor = conn.cursor()
                cursor.execute("PRAGMA foreign_keys = ON;")
                cursor.execute(sql, values)

                if fetchone:
                    return cursor.fetchone()

                if cursor.rowcount > 0:
                    conn.commit()
                    return True
                elif cursor.rowcount <= 0:
                    return False

                return True
        except sqlite3.OperationalError as error:
            self.log.error(f"OperationalError: {error}")
            return False
        except sqlite3.IntegrityError as error:
            if values[0] == 'admin' and str(error) == "UNIQUE constraint failed: users.user_name":
                self.log.info('Admin registered')
                return False
            else:
                self.log.error(f"IntegrityError: {error}")
                return False
        except Exception as error:
            self.log.error(f"Unexpected error: {error}")
            return False

    def create_tables(self):
        commands = [
            """CREATE TABLE IF NOT EXISTS users (
                   user_id INTEGER PRIMARY KEY AUTOINCREMENT,
                   user_name TEXT NOT NULL UNIQUE,
                   password TEXT NOT NULL,
                   role INTEGER NOT NULL DEFAULT 1
                );""",
            """CREATE TABLE IF NOT EXISTS messages (
                   message_id INTEGER PRIMARY KEY AUTOINCREMENT,
                   message TEXT NOT NULL DEFAULT '',
                   created_at DATETIME NOT NULL,  -- No default, must provide
                   sender TEXT NOT NULL DEFAULT '',
                   receiver TEXT NOT NULL DEFAULT '',
                   FOREIGN KEY (receiver) REFERENCES users(user_name) ON DELETE CASCADE
                );""",
        ]
        # CURRENT_TIMESTAMP
        for command in commands:
            if not self._execute_sql(command):
                self.log.info("Tables already created")
                return

        self.log.info('Tables created successfully')

    def insert_admin(self):
        sql = """INSERT INTO users(user_name, password, role) VALUES(?,?,?)"""
        values = ("admin", "$2b$12$hXPu5NPx6cVbFE9AoJsdXuK8Ktai6JTYXpRGQ6hmFVOg5oQDhBUFa", 0)
        if self._execute_sql(sql, values):
            self.log.info("Admin registered")


    def insert_user_sqlite(self, user_name, password, role):
        sql = """INSERT INTO users(user_name, password, role) VALUES(?,?,?)"""
        values = (user_name, password, role)
        result = self._execute_sql(sql, values)
        if result == True:
            self.log.info(f"User {user_name} registered")
            return True
        elif result == "UNIQUE constraint failed: users.user_name":
            self.log.info(f"Attempt to create user with existing username - {user_name}")
            return result
        return False

    def delete_msg_by_id_sqlite(self, username, msg_id):
        sql = """DELETE FROM messages WHERE receiver = ? and message_id = ?;"""
        result =  self._execute_sql(sql, (username, msg_id))
        if result == True:
            self.log.info(f"Usunięto wiadomości Użytkownika {username}.")
            return True
        else:
            return False

    def delete_msg_older_than_sqlite(self, username, days: str):
        sql = """DELETE FROM messages WHERE receiver = ? AND created_at < DATETIME('now', ?);"""
        values = (username, days)
        result = self._execute_sql(sql, values)
        if result == True:
            self.log.info(f"Usunięto wiadomości Użytkownika {username}.")
            return True
        else:
            return False

    def delete_all_msg_of_user_sqlite(self, username):
        sql_all = """DELETE FROM messages WHERE receiver = ?"""
        values = (username,)
        result = self._execute_sql(sql_all, values)
        if result == True:
            self.log.info(f"Usunięto wiadomości Użytkownika {username}.")
            return True
        else:
            return False

    def check_user(self, receiver):
        sql = """select 1 from users where user_name = ?;"""
        values = (receiver,)
        result = self._execute_sql(sql, values, fetchone=True)
        if result == (1,):
            self.log.info("Check user - True")
            return True
        else:
            self.log.info("Check user - False")
            return False

    def save_message(self, sender, message, receiver):
        sql = """INSERT INTO messages (sender, message, receiver, created_at) VALUES (?, ?, ?, ?);"""
        values = (sender, message, receiver, '2024-12-01 18:00:00')
        result = self._execute_sql(sql, values)
        if result == True:
            self.log.info(f"Zapisano wiadomość.")
            return True
        else:
            return False

    def delete_user_sqlite(self, username):
        sql = """DELETE FROM users WHERE user_name = ?;"""
        value = username
        result = self._execute_sql(sql, (value,))
        if result == True:
            self.log.info(f"User {username} deleted")

    def print_msg_sqlite(self, user, all_msg=False):
        sql_f = """select message, sender, receiver, message_id, created_at 
        from messages where receiver = ? order by message_id desc;"""
        sql_t = """select message, sender, receiver, message_id, created_at 
        from messages order by message_id desc;"""
        try:
            with sqlite3.connect(self.database) as conn:
                cursor = conn.cursor()
                if all_msg:
                    cursor.execute(sql_t)
                else:
                    cursor.execute(sql_f, (user,))
                rows = cursor.fetchall()
                if not rows:
                    return False
                i = 1
                msg_str = "Wiadomosci:\n"
                for message in rows:
                    string = (  # testy świrują z tymi stringami - tak przechodzą
                        f"Wiadomosc -{i}- od: {message[1]} - do: {message[2]}\n"
                        f"Wysłana: {message[4]}\n"
                        f"Wiadomosc [id:{message[3]}]:\n{message[0]}\n\n"
                    )
                    i += 1
                    msg_str += string
                return True, msg_str
        except sqlite3.OperationalError as error:
            self.log.error(f"ErrorDB: {error}")
            return False
        except sqlite3.IntegrityError as error:
            self.log.error(f"ErrorDB: {error}")
            return False
        except Exception as error:
            self.log.error(f"ErrorDB: {error}")
            return False


    def log_user_sqlite(self, username, password):
        sql = """select user_name, password, role from users where user_name = ?;"""
        value = username
        try:
            with sqlite3.connect(self.database) as conn:
                cursor = conn.cursor()
                cursor.execute(sql, (value,))
                result = cursor.fetchone()
                if result:
                    db_username, db_password, db_role = result
                    hashed_password = db_password.encode("utf-8")
                    if self.check_password(hashed_password, password):
                        self.log.info(f"Zalogowano - {username}")
                        return True, result
                    else:
                        self.log.warning(f"Błąd logowania. Niepoprawne hasło")
                        return False
                else:
                    self.log.warning(f"Błąd logowania. Niepoprawny login")
                    return False
        except sqlite3.OperationalError as error:
            self.log.error(f"ErrorDB: {error}")
            return False
        except sqlite3.IntegrityError as error:
            self.log.error(f"ErrorDB: {error}")
            return False
        except Exception as error:
            self.log.error(f"ErrorDB: {error}")
            return False

    def check_user_role(self, receiver, role):
        sql = """select 1 from users where user_name = ? and role = ?;"""
        values = (receiver, role)
        result = self._execute_sql(sql, values, fetchone=True)
        if result == (1,):
            return True
        else:
            return False

    def print_user_sqlite(self):
        sql = """select user_name from users order by user_name;"""
        users_str = """"""
        try:
            with sqlite3.connect(self.database) as conn:
                cursor = conn.cursor()
                cursor.execute(sql)
                rows = cursor.fetchall()
                i = 1
                for row in rows:
                    string = f"{i} - {row[0]}\n"
                    i += 1
                    users_str += string
        except sqlite3.OperationalError as error:
            print(f"Operational error: {error}")
            return False
        except sqlite3.IntegrityError as error:
            print(f"Integrity error: {error}")
            return False
        except Exception as error:
            print(f"Unexpected error: {error}")
            return False
        finally:
            return users_str

