


import json
with open('config.json') as config_file:
    config = json.load(config_file)
HOST = config['host']
PORT = config['port']

from getpass import getpass
from os import system, name

import logging
logging.basicConfig(
    filename="logs_file.log",
    encoding="utf-8",
    filemode="a",
    format="{asctime} - {levelname} - {name} - {message}",
    style="{",
    datefmt="%Y-%m-%d %H:%M",
    level=logging.DEBUG,
)
from icecream import ic
ic.configureOutput(prefix='Client class - ')


def clear():
    """A function that clears the terminal between commands"""
    if name == "nt":
        system("cls")
    else:
        system("clear")
    print("")


class Client:
    def __init__(self, host=HOST, port=PORT, logger_name='Client'):
        self.host = host
        self.port = port
        self.client_connected = True

        self.log = logging.getLogger(logger_name)
        self.log.info("Connected")



    def send_to_server(self, sock):
        """Basic function for sending messages to the server"""
        message = input("\nWpisz komende serwera ('help' - pomoc) ")
        clear()
        data = {"cmd": message}
        sock.sendall(json.dumps(data).encode("utf-8"))

    def receive_from_server(self, sock):
        """Basic function for receiving messages from the server"""
        response_data = sock.recv(1024).decode("utf-8")
        response = json.loads(response_data)
        print(f'\nOdpowiedz z serwera:\n{response["response"]}')
        return response



    @staticmethod
    def reg(sock):
        """Function for registering a new client on the server"""
        registration_data = json.dumps(
            {
                "cmd": "register",
                "user": input("Wpisz login dla użytkownika: "),
                "password": getpass("Wpisz hasło dla użytkownika: "),
                "role": 1,
            }
        )
        sock.sendall(registration_data.encode("utf-8"))

    @staticmethod
    def login(sock):
        """Function for logging into the server"""
        login_data = json.dumps(
            {
                "cmd": "log",
                "user": input("Wpisz login użytkownika: "),
                "password": getpass("Hasło: "),
            }
        )
        sock.sendall(login_data.encode("utf-8"))

    @staticmethod
    def delete_user(sock):
        """Function for deleting a user from the server"""
        login_data = json.dumps(
            {"cmd": "del", "user": input("Wpisz login użytkownika: ")}
        )
        sock.sendall(login_data.encode("utf-8"))

    @staticmethod
    def message(sock):
        """Function for sending a message to the User"""
        to_user = input("Wiadomosc do...(wpisz login użytkownika) ")
        while True:
            message = input("Tresc wiaomosci: ")
            if len(message) > 255:
                print(f"Tresc wiaomosci jest dłuższa niż 255 znaków.\n")
            else:
                message_data = json.dumps(
                    {"cmd": "receive_message", "to_user": to_user, "message": message}
                )
                sock.sendall(message_data.encode("utf-8"))
                return False

    @staticmethod
    def delete_msg(sock):
        """Auxiliary function for deleting messages from the user'server inbox"""
        all_or_id = input(f"")
        message_data = json.dumps(
            {
                "cmd": f"delete_message",
                "all_or_id": f"{all_or_id}",
            }
        )
        sock.sendall(message_data.encode("utf-8"))

