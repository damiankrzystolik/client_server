import sqlite3
import unittest
from unittest.mock import MagicMock, patch
from cs_db_sqlite_class import DataBaseSQLITE
import io
import sys

from icecream import ic
ic.configureOutput(prefix='>>> ')
# ic.configureOutput(includeContext=True)
ic.disable()  # Wyłącza wyświetlanie

import logging



class TestDatabaseSQLITE(unittest.TestCase):

    def setUp(self):
        # 0. baza danych
        self.db = DataBaseSQLITE('test_logger')

        self.log = logging.getLogger('test_logger')
        self.log.setLevel(logging.INFO)

        # 1. Utworzenie mockowanych połączeń
        self.mock_connection = MagicMock()
        self.mock_cursor = MagicMock()

        # 2. Musiałem to dopisać bo 1. nie wystarczało
        self.mock_connection.__enter__.return_value = self.mock_connection
        self.mock_connection.cursor.return_value = self.mock_cursor

    @patch('sqlite3.connect')
    def test_delete_msg_by_id_sqlite_when_msg_exist(self, mock_connect):
        mock_connect.return_value = self.mock_connection

        # 3, Ustaw oczekiwany rowcount dla kursora
        self.mock_cursor.rowcount = 1  # lub 0, jeśli chcesz przetestować przypadek nieusunięcia żadnego rekordu

        # 4. Wywołanie funkcji
        result = self.db.delete_msg_by_id_sqlite("admin", 1)
        ic(result)

        # 5. Sprawdzanie czy sqlquery się odpala z wpisanymi wartosciami.
        self.mock_cursor.execute.assert_called_once_with(
            '''DELETE FROM messages WHERE receiver = ? and message_id = ?;''',
            ('admin', 1)
        )

        # 6. sprawdzanie czy był commit do bazy
        self.mock_connection.commit.assert_called_once()

        # 7. Sprawdzanie co zwróciła funkcja
        self.assertEqual(result, True)

    @patch('sqlite3.connect')
    def test_delete_msg_by_id_sqlite_when_msg_not_exist(self, mock_connect):
        mock_connect.return_value = self.mock_connection

        # 3, Ustaw oczekiwany rowcount dla kursora
        self.mock_cursor.rowcount = 0  # lub 0, jeśli chcesz przetestować przypadek nieusunięcia żadnego rekordu

        # 4. Wywołanie funkcji
        result = self.db.delete_msg_by_id_sqlite("admin", 1)
        ic(result)

        # 5. Sprawdzanie czy sqlquery sie odpala z wpisanymi wartosciami.
        self.mock_cursor.execute.assert_called_once_with(
            '''DELETE FROM messages WHERE receiver = ? and message_id = ?;''',
            ('admin', 1)
        )

        # 6. sprawdzanie czy był commit do bazy
        self.mock_connection.commit.assert_not_called()

        # 7. Sprawdzanie co zwróciła funkcja
        self.assertEqual(result, False)


    @patch('sqlite3.connect')
    def test_insert_admin_empty(self, mock_connect):
        mock_connect.return_value = self.mock_connection


                # 3. Dodatek od asystenta. To ma za zadanie ustawić pustą baze do tego testu.
                #  Bez tego nie działało, ale jak to zakomentowalem to dalej działą.
        # Prepare the mock cursor and set the return of the fetch to simulate an empty table
        # mock_cursor.execute.side_effect = [
        #     None,  # Simulate CREATE TABLE or any setup command
        #     None  # Simulate INSERT command
        # ]
        # mock_cursor.fetchone.return_value = None

        # 4. Wywołanie funkcji
        result = self.db.insert_admin()

        # 5. Sprawdzanie czy sqlquery sie odpala z wpisanymi wartosciami.
        self.mock_cursor.execute.assert_called_once_with(
            '''INSERT INTO users(user_name, password, role) VALUES(?,?,?)''',
            ('admin', '$2b$12$hXPu5NPx6cVbFE9AoJsdXuK8Ktai6JTYXpRGQ6hmFVOg5oQDhBUFa', 0)
        )

        # 6. sprawdzanie czy był commit do bazy
        self.mock_connection.commit.assert_called_once()

        # 7. Sprawdzanie co zwróciła funkcja
        self.assertEqual(result, 'Zarejestrowano nowego Admina.')


    @patch('sqlite3.connect')
    def test_insert_admin_exist(self, mock_connect):
        mock_connect.return_value = self.mock_connection

        # 1. Wywołuje efekt po odpaleniu sqla
        def execute_side_effect(sql, params):
            if "INSERT INTO users" in sql:
                raise sqlite3.IntegrityError("UNIQUE constraint failed: users.user_name")

        self.mock_cursor.execute.side_effect = execute_side_effect

        # 2. Podpowiedz gpt. Wyłapuje stringi.
        captured_output = io.StringIO()
        sys.stdout = captured_output
        try:
            result = self.db.insert_admin()
        except Exception as e:
            # Reset redirect.
            sys.stdout = sys.__stdout__
            raise e
        # Reset stdout
        sys.stdout = sys.__stdout__

        # 3. Spr. query
        self.mock_cursor.execute.assert_called_once_with(
            '''INSERT INTO users(user_name, password, role) VALUES(?,?,?)''',
            ('admin', '$2b$12$hXPu5NPx6cVbFE9AoJsdXuK8Ktai6JTYXpRGQ6hmFVOg5oQDhBUFa', 0)
        )

        # 4, Spr czy commit się NIE odpala
        self.mock_connection.commit.assert_not_called()
        ic(captured_output.getvalue())

        # 5. Wyłapanie stringa w tym błędzie
        self.assertIn("Admin zarejestrowany.", captured_output.getvalue())
        ic(result)
        ic(sys.stdout)

        # 6. Spr czy zwracana jest poprawna wartosć funkcji. Nic nie jest wpisywane do dazy. Nic nie jest zwracane. Więc result powinno być None.
        self.assertEqual(result, None)

    @patch('sqlite3.connect')
    def test_insert_admin_error(self, mock_connect):
        mock_connect.return_value = self.mock_connection

        # Test błędu. Wersja 1. oparta o logi
        with self.assertLogs('test_logger', level='ERROR') as log:
            self.mock_cursor.execute.side_effect = sqlite3.IntegrityError()
            result = self.db.insert_admin()
            ic(result)

            ic(log.output[0])
            self.assertIn('ErrorDB: ', log.output[0])

            self.mock_cursor.execute.assert_called_once_with(
                '''INSERT INTO users(user_name, password, role) VALUES(?,?,?)''',
                ('admin', '$2b$12$hXPu5NPx6cVbFE9AoJsdXuK8Ktai6JTYXpRGQ6hmFVOg5oQDhBUFa', 0)
            )

            # 4, Spr czy commit się NIE odpala
            self.mock_connection.commit.assert_not_called()

            # 6. Spr czy zwracana jest poprawna wartosć funkcji. Nic nie jest wpisywane do dazy.
            self.assertEqual(result, False)


        # # Test błędu. Wersja 2.
        # # 1. funkcja do wyrzucania errorów
        # def execute_side_effect(sql, params):
        #     if "INSERT INTO users" in sql:
        #         raise sqlite3.IntegrityError()
        #
        # self.mock_cursor.execute.side_effect = execute_side_effect
        #
        # # 2. Podpowiedz gpt. Wyłapuje stringi.
        # captured_output = io.StringIO()
        # sys.stdout = captured_output
        # try:
        #     result = self.db.insert_admin()
        # except Exception as e:
        #     # Reset redirect.
        #     sys.stdout = sys.__stdout__
        #     raise e
        # # Reset stdout
        # sys.stdout = sys.__stdout__
        # ic(sys.stdout)
        #
        # # 3. Spr. query
        # self.mock_cursor.execute.assert_called_once_with(
        #     '''INSERT INTO users(user_name, password, role) VALUES(?,?,?)''',
        #     ('admin', '$2b$12$hXPu5NPx6cVbFE9AoJsdXuK8Ktai6JTYXpRGQ6hmFVOg5oQDhBUFa', 0)
        # )
        #
        # # 4, Spr czy commit się NIE odpala
        # self.mock_connection.commit.assert_not_called()
        #
        # # 6. Spr czy zwracana jest poprawna wartosć funkcji. Nic nie jest wpisywane do dazy. Nic nie jest zwracane. Więc result powinno być None.
        # ic(result)
        # self.assertEqual(result, False)


    @patch('sqlite3.connect')
    def test_delete_msg_older_than(self, mock_connect):
        mock_connect.return_value = self.mock_connection

        # 3, Ustaw oczekiwany rowcount dla kursora
        self.mock_cursor.rowcount = 1  # lub 0, jeśli chcesz przetestować przypadek nieusunięcia żadnego rekordu

        # 4. Wywołanie funkcji
        result = self.db.delete_msg_older_than_sqlite("admin", "1")
        ic(result)

        # 5. Sprawdzanie czy sqlquery sie odpala z wpisanymi wartosciami.
        self.mock_cursor.execute.assert_called_once_with(
            '''DELETE FROM messages WHERE receiver = ? AND created_at < DATETIME('now', ?);''',
            ('admin', "1")
        )

        # 6. sprawdzanie czy był commit do bazy
        self.mock_connection.commit.assert_called_once()

        # 7. Sprawdzanie co zwróciła funkcja
        self.assertEqual(result, True)

    @patch('sqlite3.connect')
    def test_delete_msg_older_than_no_msg(self, mock_connect):
        mock_connect.return_value = self.mock_connection

        # 3, Ustaw oczekiwany rowcount dla kursora
        self.mock_cursor.rowcount = 0  # lub 0, jeśli chcesz przetestować przypadek nieusunięcia żadnego rekordu

        # 4. Wywołanie funkcji
        result = self.db.delete_msg_older_than_sqlite("admin", "1")
        ic(result)

        # 5. Sprawdzanie czy sqlquery sie odpala z wpisanymi wartosciami.
        self.mock_cursor.execute.assert_called_once_with(
            '''DELETE FROM messages WHERE receiver = ? AND created_at < DATETIME('now', ?);''',
            ('admin', "1")
        )

        # 6. sprawdzanie czy był commit do bazy
        self.mock_connection.commit.assert_not_called()

        # 7. Sprawdzanie co zwróciła funkcja
        self.assertEqual(result, False)


    @patch('sqlite3.connect')
    def test_delete_all_msg_of_user(self, mock_connect):
        mock_connect.return_value = self.mock_connection

        # 3, Ustaw oczekiwany rowcount dla kursora
        self.mock_cursor.rowcount = 1  # lub 0, jeśli chcesz przetestować przypadek nieusunięcia żadnego rekordu

        # 4. Wywołanie funkcji
        result = self.db.delete_all_msg_of_user_sqlite("admin")
        ic(result)

        # 5. Sprawdzanie czy sqlquery sie odpala z wpisanymi wartosciami.
        self.mock_cursor.execute.assert_called_once_with(
            '''DELETE FROM messages WHERE receiver = ?''',
            ('admin',)
        )

        # 6. sprawdzanie czy był commit do bazy
        self.mock_connection.commit.assert_called_once()

        # 7. Sprawdzanie co zwróciła funkcja
        self.assertEqual(result, True)

    @patch('sqlite3.connect')
    def test_delete_all_msg_of_user_no_msg(self, mock_connect):
        mock_connect.return_value = self.mock_connection

        # 3, Ustaw oczekiwany rowcount dla kursora
        self.mock_cursor.rowcount = 0  # lub 0, jeśli chcesz przetestować przypadek nieusunięcia żadnego rekordu

        # 4. Wywołanie funkcji
        result = self.db.delete_all_msg_of_user_sqlite("admin")
        ic(result)

        # 5. Sprawdzanie czy sqlquery sie odpala z wpisanymi wartosciami.
        self.mock_cursor.execute.assert_called_once_with(
            '''DELETE FROM messages WHERE receiver = ?''',
            ('admin',)
        )

        # 6. sprawdzanie czy był commit do bazy
        self.mock_connection.commit.assert_not_called()

        # 7. Sprawdzanie co zwróciła funkcja
        self.assertEqual(result, False)

    @patch('sqlite3.connect')
    def test_check_user(self, mock_connect):
        mock_connect.return_value = self.mock_connection

        self.mock_cursor.fetchone.return_value = (1,)

        # 4. Wywołanie funkcji
        result = self.db.check_user("admin")


        # 5. Sprawdzanie czy sqlquery sie odpala z wpisanymi wartosciami.
        self.mock_cursor.execute.assert_called_once_with(
            '''select 1 from users where user_name = ?;''',
            ('admin',)
        )

        # 7. Sprawdzanie co zwróciła funkcja
        self.assertEqual(result, True)

    @patch('sqlite3.connect')
    def test_check_user_no_user(self, mock_connect):
        mock_connect.return_value = self.mock_connection

        self.mock_cursor.fetchone.return_value = None

        # 4. Wywołanie funkcji
        result = self.db.check_user("admin")
        ic(result)

        # 5. Sprawdzanie czy sqlquery sie odpala z wpisanymi wartosciami.
        self.mock_cursor.execute.assert_called_once_with(
            '''select 1 from users where user_name = ?;''',
            ('admin',)
        )

        # 7. Sprawdzanie co zwróciła funkcja
        self.assertEqual(result, False)

    @patch('sqlite3.connect')
    def test_save_massage(self, mock_connect):
        mock_connect.return_value = self.mock_connection

        self.mock_cursor.rowcount = 1

        # 4. Wywołanie funkcji
        result = self.db.save_massage('dam', 'test', "admin")
        ic(result)

        # 5. Sprawdzanie czy sqlquery sie odpala z wpisanymi wartosciami.
        self.mock_cursor.execute.assert_called_once_with(
            '''INSERT INTO messages (sender, message, receiver) VALUES (?, ?, ?);''',
            ('dam', 'test', "admin")
        )

        # 7. Sprawdzanie co zwróciła funkcja
        self.assertEqual(result, True)

    @patch('sqlite3.connect')
    def test_save_massage_false(self, mock_connect):
        mock_connect.return_value = self.mock_connection

        self.mock_cursor.rowcount = 1

        # 4. Wywołanie funkcji
        result = self.db.save_massage('dam', 'test', "admin")
        ic(result)

        # 5. Sprawdzanie czy sqlquery sie odpala z wpisanymi wartosciami.
        self.mock_cursor.execute.assert_called_once_with(
            '''INSERT INTO messages (sender, message, receiver) VALUES (?, ?, ?);''',
            ('dam', 'test', "admin")
        )

        # 6. sprawdzanie czy był commit do bazy
        self.mock_connection.commit.assert_called_once()

        # 7. Sprawdzanie co zwróciła funkcja
        self.assertEqual(result, True)

    @patch('sqlite3.connect')
    def test_print_msg(self, mock_connect):
        mock_connect.return_value = self.mock_connection

        # self.mock_cursor.fetchone.return_value = [] # no messages
        self.mock_cursor.fetchall.return_value = [('test4', 'admin', 'admin', 3, '2024-12-03 19:54:39')]

        # 4. Wywołanie funkcji
        result = self.db.print_msg_sqlite("admin")
        ic(result)

        # 5. Sprawdzanie czy sqlquery sie odpala z wpisanymi wartosciami.
        self.mock_cursor.execute.assert_called_once_with(
            '''select message, sender, receiver, message_id, created_at 
        from messages where receiver = ? order by message_id desc;''',
            ('admin',)
        )

        # 7. Sprawdzanie co zwróciła funkcja
        self.assertEqual(result, (True, f'''Wiadomosci:\nWiadomosc -1- od: admin - do: admin\nWysłana: 2024-12-03 19:54:39\nWiadomosc [id:3]:\ntest4
'''))

    @patch('sqlite3.connect')
    def test_print_msg_no_msg(self, mock_connect):
        mock_connect.return_value = self.mock_connection

        self.mock_cursor.fetchall.return_value = [] # no messages
        # self.mock_cursor.fetchall.return_value = [('test4', 'admin', 'admin', 3, '2024-12-03 19:54:39')]

        # 4. Wywołanie funkcji
        result = self.db.print_msg_sqlite("admin")
        ic(result)

        # 5. Sprawdzanie czy sqlquery sie odpala z wpisanymi wartosciami.
        self.mock_cursor.execute.assert_called_once_with(
            '''select message, sender, receiver, message_id, created_at 
        from messages where receiver = ? order by message_id desc;''',
            ('admin',)
        )

        # 7. Sprawdzanie co zwróciła funkcja
        self.assertEqual(result, False)

    @patch.object(DataBaseSQLITE, 'check_password', return_value=True)
    @patch('sqlite3.connect')
    def test_log_user_correct_password(self, mock_connect, mock_check_password):
        mock_connect.return_value = self.mock_connection

        expected_result = ('admin', '$2b$12$hXPu5NPx6cVbFE9AoJsdXuK8Ktai6JTYXpRGQ6hmFVOg5oQDhBUFa', 0)
        self.mock_cursor.fetchone.return_value = expected_result


        # 4. Wywołanie funkcji
        result, user_data = self.db.log_user_sqlite('admin', 'correct_password')

        self.assertTrue(result)
        self.assertEqual(user_data, expected_result)
        mock_check_password.assert_called_once_with(expected_result[1].encode("utf-8"), 'correct_password')

    @patch.object(DataBaseSQLITE, 'check_password', return_value=False) # tu mockujemy złe hasło
    @patch('sqlite3.connect')
    def test_log_user_incorrect_password(self, mock_connect, mock_check_password):
        mock_connect.return_value = self.mock_connection

        expected_result = ('admin', '$2b$12$hXPu5NPx6cVbFE9AoJsdXuK8Ktai6JTYXpRGQ6hmFVOg5oQDhBUFa', 0)
        self.mock_cursor.fetchone.return_value = expected_result

        # 4. Wywołanie funkcji
        result = self.db.log_user_sqlite('admin', 'incorrect_password')
        ic(result)

        self.assertFalse(result)
        mock_check_password.assert_called_once_with(expected_result[1].encode("utf-8"), 'incorrect_password')

    @patch('sqlite3.connect')
    def test_log_user_no_user(self, mock_connect):
        mock_connect.return_value = self.mock_connection

        self.mock_cursor.fetchone.return_value = None

        # 4. Wywołanie funkcji
        result = self.db.log_user_sqlite('invalid_user', 'correct_password')

        self.assertFalse(result)


    @patch('sqlite3.connect')
    def test_delete_user_sqlite(self, mock_connect):
        # funkcja jest odpalana tylko jezeli użytkownik istnieje i nie jest adminem
        # jeden test wystarczy
        mock_connect.return_value = self.mock_connection

        # 4. Wywołanie funkcji
        result = self.db.delete_user_sqlite('admin')
        ic(result)
        # Użycie `assert_any_call` pozwala na sprawdzenie, czy funkcja była wywołana z danym argumentem przynajmniej raz.
        self.mock_cursor.execute.assert_any_call("PRAGMA foreign_keys = ON;")

        # 5. Sprawdzanie czy sqlquery sie odpala z wpisanymi wartosciami.
        self.mock_cursor.execute.assert_any_call(
            '''DELETE FROM users WHERE user_name = ?;''',
            ('admin',)
        )

        # 6. sprawdzanie czy był commit do bazy
        self.mock_connection.commit.assert_called_once()

    @patch('sqlite3.connect')
    def test_check_user_role_true(self, mock_connect):
        mock_connect.return_value = self.mock_connection

        self.mock_cursor.fetchone.return_value = (1,)

        result = self.db.check_user_role('test_user', 1)
        ic(result)

        self.mock_cursor.execute.assert_called_once_with("""select 1 from users where user_name = ? and role = ?;""", ('test_user', 1))

        self.assertTrue(result)

    @patch('sqlite3.connect')
    def test_check_user_role_false(self, mock_connect):
        mock_connect.return_value = self.mock_connection

        self.mock_cursor.fetchone.return_value = None

        result = self.db.check_user_role('test_user', 1)
        ic(result)

        self.mock_cursor.execute.assert_called_once_with("""select 1 from users where user_name = ? and role = ?;""", ('test_user', 1))

        self.assertFalse(result)







# Klasa do sprawdzania standardowych bloków except dla wiekszości funkcji
class TestDatabaseMethodsErrors(unittest.TestCase):

    def setUp(self):
        self.db = DataBaseSQLITE('test_logger')
    def test_error_handling(self):
        # możliwości errorów i odpowiedzi. W odpowiedniej kolejności.
        errors = (sqlite3.IntegrityError, sqlite3.OperationalError, Exception)
        answers = ('ErrorDB: ','ErrorDB: ','ErrorDB: ')

        # Funkcje i ich argumenty
        test_cases = [
            (self.db.check_user, ('admin',)),
            (self.db.delete_msg_by_id_sqlite, ('admin', 1)),
            (self.db.delete_msg_older_than_sqlite, ('admin', '1')),
            (self.db.delete_all_msg_of_user_sqlite, ('admin',)),
            (self.db.save_massage, ('dam', 'test 33', 'admin' )),
            (self.db.delete_msg_by_id_sqlite,('admin', 1)),
            (self.db.print_msg_sqlite,('admin',)),
            (self.db.log_user_sqlite, ('admin', 'admin'))
            # Tuple with function and its args
            # Add more test cases here
            # Example: (self.db.delete_msg_by_id_sqlite, ('admin', 1))
        ]

        # Główna pętla w której wszystko się składa i odpala
        for func, args in test_cases:
            for error, answer in zip(errors, answers):
                with self.subTest(func=func, error=error):
                    self.run_error_test(func, args, error, answer)


    # funkcja z właściwym testem.
    def run_error_test(self, func, args, error, expected_message):
        with patch('sqlite3.connect') as mock_connect:
            # Create mock cursor and connection
            mock_connection = MagicMock()
            mock_cursor = MagicMock()
            mock_connect.return_value = mock_connection
            mock_connection.__enter__.return_value = mock_connection
            mock_connection.cursor.return_value = mock_cursor


            with self.assertLogs('test_logger', level='ERROR') as log:
                mock_cursor.execute.side_effect = error
                result = func(*args)
                ic(result)

                ic(log.output[0])
                self.assertIn('ErrorDB: ', log.output[0])

                # 6. Spr czy zwracana jest poprawna wartosć funkcji. Nic nie jest wpisywane do dazy.
                self.assertEqual(result, False)

if __name__ == '__main__':
    unittest.main()
